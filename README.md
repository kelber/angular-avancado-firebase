# Consultoria App

Aplicação utilizando metodologia avançada do Angular na versão 9

1 - Criação de componentes visuais baseados no google material, porém feitos todos *manualmente*.

2 - Criação de módulos independentes para reuso

3 - Implementação de backend BASS - Firebase com authenticação e controle de rotas.

4 - Utilização da biblioteca NGRX para controle de estado de usuário. 

5 - Upload de fotos para backend e tratamento de imagem com a biblioteca ngx-image-cropper. entre outros...



link da aplicação em funcionamento.

https://angular-advanced-55541.web.app/static/welcome



#### Instalação

> npm install

> npm run start  

ou

> npm run start:dev

