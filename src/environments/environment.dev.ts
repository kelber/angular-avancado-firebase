export const environment = {
    production: false,
    name: 'dev',
    firebase: {
        config: {
            apiKey: "AIzaSyBwNqOqTyDwLy4dBOU503jyEDXchgbcIoU",
            authDomain: "angular-advanced-55541.firebaseapp.com",
            projectId: "angular-advanced-55541",
            storageBucket: "angular-advanced-55541.appspot.com",
            messagingSenderId: "386169701520",
            appId: "1:386169701520:web:9715434e2f61669593c415"
        },
        actionCodeSettings: {
            url: 'http://localhost:5200/profile/new',
            handleCodeInApp: true
        }
    }
};
